import bs4, requests, sys, pprint
headers1 = ['Code', 'Issues', 'Users']
headers2 = ['Name', 'Description', 'Language', 'Stars', 'Forks', 'Last Update']
search_url = 'https://github.com/search?q='
uperDict = {}; downDict = {}; listOfNumbers = []
numbers = []; keys = []

if len(sys.argv) > 1:

    key = ' '.join(sys.argv[1:])
    res = requests.get(search_url+key+'+')
    soup = bs4.BeautifulSoup(res.text, 'html.parser')
    items = soup.findAll('li', class_='repo-list-item public source')
    
    # the up items of menu
    menu = soup.findAll('nav', class_='menu')
    data = menu[0].findAll('include-fragment')
    
    for d in data:   
        req = d.get('src')
        anotherRes = requests.get(req)
        anotherSoup = bs4.BeautifulSoup(anotherRes.text, 'html.parser')
        listOfNumbers.append(anotherSoup.text)

    uperDict = dict(zip(headers1, listOfNumbers))
    #print(uperDict)

    #the down items of menu
    bar = soup.find('ul', class_='filter-list small').text.split()
    for i in range(len(bar)):
       
        if i % 2 == 0:
            numbers.append(bar[i])
        else:
            keys.append(bar[i])
    downDict = dict(zip(keys, numbers))
    
    
    #copied it from AutomatedStuff
    def printItemsVert(t, itemsDict, lWidth, rWidth):
        print ('\n')
        print (t.center(lWidth+rWidth, '-'))
        for k, v  in itemsDict.items():
            print(k.ljust(lWidth, '.') + str(v).rjust(rWidth))
        print('\n') 
    
  # def printItemsHor(t, itemsDict, spacing):
  #     print(t.center(3*spacing))
  #     for k in itemsDict.keys():
  #         print(k.center(spacing, ' '), end =' ')
  #     print ('\n')

    
    myDict = {}
    
    for item in items:
        name = item.find('h3', class_='repo-list-name').text.strip()
        desc = item.find('p', class_='repo-list-description').text.strip()
        nav = item.find('div', class_='repo-list-stats').text.split()
        date = item.find('p', class_='repo-list-meta').text.split()
        lastUp = ' '.join(date)
        myDict[name] = []
        if len(nav) == 3:
            lang = nav[0]; stars = nav[1]; forks = nav[2]
        else:
            lang = 'Not specified'; stars = nav[0]; forks = nav[1]

        anotherDict = dict(zip(headers2, [name, desc, lang, stars, forks, lastUp]))
        myDict[name].append(anotherDict)
             
    #the printing part
    sentence = soup.findAll('h3')
    title = sentence[1].text.strip()     
    printItemsVert(title,uperDict, 20, 5)
    
    # This is the dictionary that stores informations about contributors  
    pprint.pprint(myDict)
    
    printItemsVert('Language',downDict, 20, 5)
    
else:
    print ('no keyword given, ex: python3 scrapGith.py css')

