import random
HANGMANPICS = ['''
        +---+
        |   |
            |
            |
            |
            |
     ========= ''', '''
        +---+
        |   |
        O   |
            |
            |
            |
     ========= ''','''
        +---+
        |   |
        0   |
        |   |
            |
            |
     ========= ''', '''
         +---+
         |   |
        [O   |
         |   |
             |
             |
     ========= ''', '''
         +---+
         |   |
        [O]  |
        /|   |
             |
             |
     ========= ''', '''
         +---+
         |   |
        [O]  |
        /|\  |
             |
             |
     ========= ''', '''
         +---+
         |   |
        [O]  |
        /|\  |
        /    |
             |
     ========= ''', '''
         +---+
         |   |
        [O]  |
        /|\  |
        / \  |
             |
     
     ========= ''' ]

words = {'Colors': 'red blue green orange yellow black violet white'.split(),
        'Shapes': 'square triangle circle hexagon rectangle ellipse chevron'.split(),
        'Fruits': 'apple orange lemon pear grape watermelon cherry banana mango tomato'.split(),
        'Animals': 'bat bear cat dog duck donkey eagle panda turkey tiger sheep rat frog'.split()}


def getRandomWord(wordDict):
    #This function returns a random string from the passed dictionary of lists of strings, and the key also
    #First, randomly select a key from the dictionary
    wordKey = random.choice(list(wordDict.keys()))
    
    #Second, randomly select a word from the key's list in the dictionary
    wordIndex = random.randint(0, len(wordDict[wordKey]) -1)
    return [wordDict[wordKey][wordIndex], wordKey]

def displayBoard(HANGMANPICS, missedLetters, correctLetters, secretWord):
    print(HANGMANPICS[len(missedLetters)])
    print ()
    print('Missed letters:', end=' ')
    for letter in missedLetters:
        print(letter, end=' ')
    print ()

    blanks = '_' * len(secretWord)
    for i in range(len(secretWord)):  #Replace blanks with correctly guessed letters
        if secretWord[i] in correctLetters:
            blanks = blanks[:i] + secretWord[i] + blanks[i+1:]

    for letter in blanks:   #show the secret word with spaces in between
        print(letter, end=' ')
    print ()

def getGuess(alreadyGuessed):
    #Returns the letter the player entered. to make sure the player entered a letter
    while True:
        print('Guess a letter')
        guess = input()
        guess = guess.lower()
        if len(guess) != 1:
            print('Please enter a single letter')
        elif guess in alreadyGuessed:
            print('You have already guessed that letter, choose again!')
        elif guess not in 'abdcdefghijklmnopqrstuvwxyz':
            print('Please enter a LETTER')
        else:
            return guess

def playAgain():
    #This function returns True if the player wants to play again
    print('Do you want to play again? (Yes or no)')
    return input().lower().startswith('y')


print ('H A N G M A N')
missedLetters = ''
correctLetters = ''
secretWord, secretKey = getRandomWord(words)
gameIsDone = False

while True:
    print('The secret word is in the set: ' + secretKey)
    displayBoard(HANGMANPICS, missedLetters, correctLetters, secretWord)

    #Let the player type in a letter
    guess = getGuess(missedLetters + correctLetters)
    
    if guess in secretWord:
        correctLetters = correctLetters + guess

        #Check if the player has won
        foundAllLetters = True
        for i in range(len(secretWord)):
            if secretWord[i] not in correctLetters:
                foundAllLetters = False
                break
        if foundAllLetters:
            print('Yes! The secret word is "'+secretWord+'"! You have won!')
            gameIsDone = True
    else:
        missedLetters = missedLetters + guess

        #check if player has guessed too many times and lost
        if len(missedLetters) == len(HANGMANPICS) -1:
            displayBoard(HANGMANPICS, missedLetters, correctLetters, secretWord)
            print('You have run out of guesses!\nAfter '+str(len(missedLetters))+ ' missed guesses and '+str(len(correctLetters)) +' correct guesses, the word was "'+secretWord+'"')
            gameIsDone = True
    
    #Ask the player if they want to play again, but only if the game is done
    if gameIsDone:
        if playAgain():
            missedLetters = ''
            correctLetters = ''
            gameIsDone = False
            secretWord, secretKey = getRandomWord(words)
        else:
            break
