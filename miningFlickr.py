import requests, json, sys, os

img_url = 'http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg'
key = '9fbf3ff1a496cd1c419d30e54c518bcb'
secret = '62ee338e1c8862b5'
SEARCH_API_URL = 'https://api.flickr.com/services/rest/'

def save_image(image, nameDir):
    os.makedirs(nameDir, exist_ok=True)
    imageFile = open(os.path.join(nameDir, os.path.basename(image.url)), 'wb')
    for chunk in image.iter_content(100000):
        print('image File:%s ' %imageFile)
        imageFile.write(chunk)
    imageFile.close()
search_text = ''.join(sys.argv[1:])
params = {
        'method': 'flickr.photos.search',
        'api_key': key,
        'text': search_text,
        'format': 'json',
        'per_page': 10,
        'nojsoncallback' : 1
        }

response = requests.get(SEARCH_API_URL, params=params).json()

for res in response['photos']['photo']:
    
    pars = {
           'farm-id' : res['farm'],
           'server-id' : res['server'],
           'id': res['id'],
           'secret': res['secret']
           }
    
    url = img_url.format(**pars)
    image = requests.get(url)
    image.raise_for_status()
    save_image(image, search_text)
