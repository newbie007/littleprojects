import requests, json, sys
import pprint
from urllib.parse import urljoin

GITHUB_API = 'https://api.github.com'
token = '1194d4915b8aaf011f29dd7b61d81720a6e35beb'
headers = {'Authorization': 'token %s' %token}
key = ''.join(sys.argv[1:])
params = {
    'page':1, 
    'per_page':10,
    'text' : key
    }

search_url =  'search/repositories?q=%s+' %key
url = urljoin(GITHUB_API,search_url)

res = requests.get(url, params = params, headers = headers) 
res.raise_for_status()
j = json.loads(res.text)
items = j['items']

for item in items:
    print('%s' %item['url'])

#print(j['items'][0]['url'])
